syn match texMathSymbol '\\arr\>' contained conceal cchar=←
syn match texMathSymbol '\\,' contained conceal cchar= 
syn match texMathSymbol '\\mathcal' contained conceal cchar= 
syn match texMathSymbol '\\text' contained conceal cchar= 
syn match texMathSymbol '\\mathbb' contained conceal cchar= 
syn match texMathSymbol '\\quad' contained conceal cchar=  
