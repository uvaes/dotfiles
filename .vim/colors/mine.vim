" Vim color file
"  Maintainer: Urbain
" Last Change: 2002/10/14 Mon 16:41.
"     version: 1.0
" This color scheme uses a light background.

set background=light
hi clear
if exists("syntax_on")
   syntax reset
endif

let colors_name = "mine"

" Syntax group
hi Comment gui=none guifg=#af5f00
hi Normal guibg=white guifg=Black
hi Type gui=none guifg=#005f00
hi Statement gui=none guifg=#5f0000
hi Special guifg=Black
hi Constant guifg=Gray
hi Error guifg=Red guibg=White
hi Preproc guifg=Blue "\end
hi Constant guifg=Green "\ $$
hi Identifier guifg=Blue "\begin
hi LineNr gui=none guifg=Gray
hi FoldColumn guibg=#F5F4FD
hi Todo guibg=white gui=underline,italic guifg=Red
hi Conceal guibg=White guifg=DarkGreen
