# dotfiles
This repository contains my configuration files.
Whenever possible, I use tools that follow the Unix philosophy.
Being a *vim* user, I find it very convenient to use tools with *vi* keybindings:

+ Window manager: **i3**
+ File manager: **vifm**
+ Browser: **uzbl**
+ PDF viewer: **zathura**
+ Email client: **mutt**
